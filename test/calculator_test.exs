defmodule CalculatorTest do
  use ExUnit.Case, async: true
  alias Calculator, as: Calc
  # doctest Calc

  setup do
    {:ok, server_pid: Calc.start()}
  end

  test "all Calculator functions", %{server_pid: server_pid} do
    assert 0 == Calc.value(server_pid)

    Calc.add(server_pid, 10)
    Calc.sub(server_pid, 5)
    Calc.mul(server_pid, 3)
    Calc.div(server_pid, 5)

    assert 3.0 === Calc.value(server_pid)
  end
end
