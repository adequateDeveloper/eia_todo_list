defmodule Calculator.ImplTest do
  use ExUnit.Case, async: true
  import ExUnit.CaptureIO
  alias Calculator.Impl, as: Calc

  # see: https://elixirschool.com/en/lessons/basics/testing/
  # see: http://blog.lucidsimple.com/2016/01/31/exunit-cheat-sheet.html

  describe "process_message/2" do
    test "with :value" do
      Calc.process_message(5, {:value, self()})
      assert_received {:response, 5}
    end

    test "with :add" do
      assert 10 == Calc.process_message(5, {:add, 5})
    end

    test "with :sub" do
      assert 5 == Calc.process_message(10, {:sub, 5})
    end

    test "with :mul" do
      assert 25 == Calc.process_message(5, {:mul, 5})
    end

    test "with :div" do
      assert 2.0 === Calc.process_message(10, {:div, 5})
    end

    test "with invalid_message" do
      assert "invalid request {:sqrt, 2}\n" ===
               capture_io(fn -> Calc.process_message(5, {:sqrt, 2}) end)
    end
  end
end
