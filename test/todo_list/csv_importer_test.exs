defmodule TodoList.CsvImporterTest do
  use ExUnit.Case, async: true
  alias TodoList.CsvImporter
  doctest CsvImporter

  test "read_lines/1 generates list of file lines" do
    expected = ["2018/12/19,Dentist\n", "2018/12/20,Shopping\n", "2018/12/19,Movies"]

    actual =
      Application.app_dir(:todo_list, "priv/todos.csv")
      |> CsvImporter.read_lines()
      |> Enum.map(& &1)

    assert actual == expected
  end

  test "trim_newlines/1 trims all newlines" do
    input = ["2018/12/19,Dentist\n", "2018/12/20,Shopping\n", "2018/12/19,Movies"]
    expected = ["2018/12/19,Dentist", "2018/12/20,Shopping", "2018/12/19,Movies"]

    actual =
      input
      |> CsvImporter.trim_newlines()
      |> Enum.map(& &1)

    assert actual == expected
  end

  test "create_entries/1 transforms list of strings to list of entry maps" do
    input = ["2018/12/19,Dentist", "2018/12/20,Shopping", "2018/12/19,Movies"]

    expected = [
      %{date: ~D[2018-12-19], title: "Dentist"},
      %{date: ~D[2018-12-20], title: "Shopping"},
      %{date: ~D[2018-12-19], title: "Movies"}
    ]

    actual =
      input
      |> CsvImporter.create_entries()
      |> Enum.map(& &1)

    assert actual == expected
  end

  test "create_entry/1 transforms tuple entry to a map entry" do
    {:ok, input_date} = Date.new(2018, 12, 19)
    input = {input_date, "Dentist"}
    expected = %{date: input_date, title: "Dentist"}

    actual =
      input
      |> CsvImporter.create_entry()

    assert actual == expected
  end

  test "extract_fields/1 transforms from string to tuple" do
    input = "2018/12/19,Dentist"
    expected = {~D[2018-12-19], "Dentist"}

    actual =
      input
      |> CsvImporter.extract_fields()

    assert actual == expected
  end

  test "transform_fields/1 transforms from list to tuple" do
    input = ["2018/12/19", "Dentist"]
    expected = {~D[2018-12-19], "Dentist"}

    actual =
      input
      |> CsvImporter.transform_fields()

    assert actual == expected
  end

  test "transform_date/1 transform string into date" do
    input = "2018/12/19"
    {:ok, expected} = Date.new(2018, 12, 19)

    actual =
      input
      |> CsvImporter.transform_date()

    assert actual == expected
  end
end
