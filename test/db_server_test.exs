defmodule DbServerTest do
  use ExUnit.Case, async: true
  alias DbServer
  # doctest DbServer

  setup do
    {:ok, server_pid: DbServer.start()}
  end

  test "start/0 returns pid", %{server_pid: server_pid} do
    assert is_pid(server_pid)
  end

  test "run_async/2 returns correct tuple", %{server_pid: server_pid} do
    client_pid = self()
    assert {:run_query, ^client_pid, "query 1"} = DbServer.run_async(server_pid, "query 1")
  end

  test "get_result/0 returns expected result", %{server_pid: server_pid} do
    DbServer.run_async(server_pid, "query 1")

    DbServer.get_result()
    |> String.contains?("query 1 result")

    DbServer.run_async(server_pid, "query 2")
    String.contains?(DbServer.get_result(), "query 2 result")
  end

  test "using multiple server processes to run concurrent queries" do
    # create a pool of dbServer processes
    pool = Enum.map(1..100, fn _ -> DbServer.start() end)

    # decide which process to run each of the 5 queries
    Enum.each(
      1..5,
      fn query_def ->
        # select a process randomly
        server_pid = Enum.at(pool, :rand.uniform(100) - 1)
        # run the query on the process
        DbServer.run_async(server_pid, query_def)
      end
    )

    # collect the responses
    responses = Enum.map(1..5, fn _ -> DbServer.get_result() end)
    IO.inspect(responses)
  end
end
