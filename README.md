# TodoList

**Following the book [Elixir in Action, 2nd Edition](https://www.manning.com/books/elixir-in-action-second-edition) by Saša Jurić**

[![pipeline status](https://gitlab.com/adequateDeveloper/eia_todo_list/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/eia_todo_list/commits/master)

## Installation

Download from the repo, and from the project root folder run:

```elixir
$ mix deps.get
```

Generate the project documentation by running from the project root folder:

```elixir
$ mix docs
```
and view them at "doc/index.html".

To get an overview of the tests, run from the project root folder:

```elixir
$ mix test --trace
```
