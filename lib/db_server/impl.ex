defmodule DbServer.Impl do
  @moduledoc """
  These module functions fall into two categories: public interface and private implementation.

  Public interface functions are executed in the caller process.
  They hide the details of process creation and the communication protocol.

  Private implementation functions run in the server process.
  """

  ## delegated client calls

  @doc """
  Start a server process that responds to messages.
  """
  @spec start() :: pid()
  def start do
    # spawn(&loop/0)
    spawn(fn ->
      loop(:rand.uniform(1000))
    end)
  end

  @doc """
  Request an asyncronous query operation.
  Sends a {:run_query} type message to a server.
  """
  @spec run_async(pid(), any()) :: any()
  def run_async(server_pid, query_def) do
    #  Note - since this called in the client process, self() is the client process id.
    send(server_pid, {:run_query, self(), query_def})
  end

  @doc """
  Retrieve server response. Timeout after 5 seconds
  """
  def get_result do
    receive do
      {:query_result, result} -> result
    after
      5000 -> {:error, :timeout}
    end
  end

  ## server implementation

  # Accepts messages from client of type {:run_query, caller pid, run query parameter}.
  # Returns messages to client of type {:query_result, query result}.
  # Note - waiting on a message puts the process in suspended state, and consequently doesn't waste CPU cycles.
  defp loop(connection) do
    receive do
      {:run_query, caller_pid, query_def} ->
        # runs the query in the server process and sends the response to the caller
        send(caller_pid, {:query_result, run_query(connection, query_def)})
    end

    loop(connection)
  end

  # query execution
  defp run_query(connection, query_def) do
    Process.sleep(2000)
    "Connection #{connection}: #{query_def} result"
  end
end
