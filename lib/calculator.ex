defmodule Calculator do
  @moduledoc """
  A simple calculator that keeps a number as state.

  ## Examples
      $ iex -S mix
      # (((0 + 10) - 5) * 3) / 5 == 3.0
      iex> calculator_pid = Calculator.start()
      iex> Calculator.value(calculator_pid)
      0
      iex> Calculator.add(calculator_pid, 10)
      iex> Calculator.sub(calculator_pid, 5)
      iex> Calculator.mul(calculator_pid, 3)
      iex> Calculator.div(calculator_pid, 5)
      iex> Calculator.value(calculator_pid)
      3.0
  """

  alias Calculator.Impl

  @doc """
    Start a server process.
  """
  defdelegate start, to: Impl

  @doc """
  Return the current value.
  A synchronous call.
  """
  defdelegate value(server_pid), to: Impl

  @doc """
  Add the `value` to the current value.
  A asynchronous call.
  """
  defdelegate add(server_pid, value), to: Impl

  @doc """
  Subtract the `value` from the current value.
  A asynchronous call.
  """
  defdelegate sub(server_pid, value), to: Impl

  @doc """
  Multiply the current value by the `value`.
  A asynchronous call.
  """
  defdelegate mul(server_pid, value), to: Impl

  @doc """
  Divide the current value by the `value`.
  A asynchronous call.
  """
  defdelegate div(server_pid, value), to: Impl
end
