defmodule TodoList.CsvImporter do
  @moduledoc """
  Import a CSV file of Todo entries into a new Todo list.
  """

  @compile if Mix.env() == :test, do: :export_all

  @doc """
  Import a CSV file of date & title rows and transform into a list of Todo entries.

  ## Examples

      iex> (
      ...>   Application.app_dir(:todo_list, "priv/todos.csv")
      ...>   |> TodoList.CsvImporter.import_todos()
      ...> )
      [
        %{date: ~D[2018-12-19], title: "Dentist"},
        %{date: ~D[2018-12-20], title: "Shopping"},
        %{date: ~D[2018-12-19], title: "Movies"}
      ]
  """

  # in: csv file of todo entries in format: 2018/12/19,Dentist\n
  # out:
  # [
  #   %{date: ~D[2018-12-19], title: "Dentist"},
  #   %{date: ~D[2018-12-20], title: "Shopping"},
  #   %{date: ~D[2018-12-19], title: "Movies"}
  # ]
  def import_todos(file_path) do
    file_path
    |> read_lines()
    |> trim_newlines()
    |> create_entries()
    |> Enum.map(& &1)
  end

  # in:  "priv/todos.csv"
  # out: ["2018/12/19,Dentist\n", "2018/12/20,Shopping\n", "2018/12/19,Movies"]
  defp read_lines(file_path) do
    file_path
    |> File.stream!()
  end

  # in:  ["2018/12/19,Dentist\n", "2018/12/20,Shopping\n", "2018/12/19,Movies"]
  # out: ["2018/12/19,Dentist", "2018/12/20,Shopping", "2018/12/19,Movies"]
  defp trim_newlines(line) do
    line
    |> Stream.map(&String.trim_trailing(&1, "\n"))
  end

  # in:  ["2018/12/19,Dentist", "2018/12/20,Shopping", "2018/12/19,Movies"]
  # out:
  # [
  #   %{date: ~D[2018-12-19], title: "Dentist"},
  #   %{date: ~D[2018-12-20], title: "Shopping"},
  #   %{date: ~D[2018-12-19], title: "Movies"}
  # ]
  defp create_entries(lines) do
    lines
    |> Stream.map(&extract_fields/1)
    |> Stream.map(&create_entry/1)
  end

  # in: {~D[2018-12-19], "Dentist"}
  # out: %{date: ~D[2018-12-19], title: "Dentist"}
  defp create_entry({date, title}) do
    %{date: date, title: title}
  end

  # in:  "2018/12/19,Dentist"
  # out: {~D[2018-12-19], "Dentist"}
  defp extract_fields(line) do
    line
    |> String.split(",")
    |> transform_fields()
  end

  # in:  ["2018/12/19", "Dentist"]
  # out: {~D[2018-12-19], "Dentist"}
  defp transform_fields([date_string, title]) do
    {transform_date(date_string), title}
  end

  # in: "2018/12/19"
  # out: ~D[2018-12-19]
  defp transform_date(date_string) do
    [year, month, day] =
      date_string
      |> String.split("/")
      |> Enum.map(&String.to_integer/1)

    {:ok, date} = Date.new(year, month, day)
    date
  end
end
