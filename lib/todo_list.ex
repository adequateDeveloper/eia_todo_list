defmodule TodoList do
  @moduledoc """
  A simple to-do list.

  The basic version of the to-do list will support the following features:
    - Creating a new data abstraction
    - Adding new entries
    - Querying the abstraction

  Eventually will be a fully working distributed web server
  that can manage a large number of to-do lists.
  """

  @doc """
  - `next_id`: ID value that is assigned to new entries.
  - `entries`: A collection of entries associated with an entry ID.
  """
  defstruct next_id: 1, entries: %{}

  alias TodoList.CsvImporter

  @doc """
  Initialize a new to-do list containing the provided list of entries.

  ## Examples

      iex> (
      ...>   [
      ...>     %{date: ~D[2018-12-19], title: "Dentist"},
      ...>     %{date: ~D[2018-12-20], title: "Shopping"},
      ...>     %{date: ~D[2018-12-19], title: "Movies"}
      ...>   ]
      ...>   |> TodoList.new()
      ...> )
      %TodoList{
        next_id: 4,
        entries: %{
          1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
          2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
          3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
        }
      }
  """
  def new(entries) when is_list(entries) do
    # Enum.reduce(entries, %TodoList{}, fn entry, todo_list_acc -> add_entry(todo_list_acc, entry) end )
    Enum.reduce(entries, %TodoList{}, &add_entry(&2, &1))
  end

  @doc """
  Initialize a new to-do list.

  ## Examples

      iex> TodoList.new
      %TodoList{next_id: 1, entries: %{}}
  """
  def new(), do: %TodoList{}

  @doc """
  Initialize a new to-do list from file.

  ## Examples

      iex> TodoList.import_entries "priv/todos.csv"
      %TodoList{
        entries: %{
          1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
          2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
          3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
        },
        next_id: 4
      }
  """
  def import_entries(filepath \\ "priv/todos.csv") do
    Application.app_dir(:todo_list, filepath)
    |> CsvImporter.import_todos()
    |> new()
  end

  @doc """
  Add an entry to the to-do list.

  `entry` is a Map containing the following keys:
  - `date:` the entry date
  - `title` the entry value.

  The entry is added to the list of entries with an auto-generated numeric key.

  ## Examples

      iex> (
      ...>  TodoList.new
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      ...> )
      %TodoList{
        next_id: 2,
        entries: %{1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"}}
      }
  """
  # To the external caller, the entire operation will be atomic.
  # Either everything will happen or, in case of an error, nothing at all. This is the consequence of immutability.
  # The effect of adding an entry is visible to others only when the add_entry/2 function finishes
  # and its result is taken into a variable.
  # If something goes wrong and you raise an error, the effect of any transformations won’t be visible.
  #
  # It’s also worth repeating, as mentioned in chapter 2, that the new to-do list (the one
  # returned by the add_entry/2 function) will share as much memory as possible with the
  # input to-do list.
  #
  def add_entry(%TodoList{} = todo_list, %{} = entry) do
    # set the new entry's ID
    # can’t use the usual %{entry | id: todo_list.next_id} because the id field is likely not already present in the map
    new_entry = Map.put(entry, :id, todo_list.next_id)

    # add the new entry to the entries collection, keyed by the current value of next_id
    new_entries = Map.put(todo_list.entries, todo_list.next_id, new_entry)

    # update the TodoList instance with the new entry and increment the next_id
    %TodoList{todo_list | entries: new_entries, next_id: todo_list.next_id + 1}
  end

  @doc """
  Replace a single entry with a new entry in the to-do list.

  `entry` is a Map containing the following keys:
  - `date`: the entry date
  - `id`: entry ID
  - `title`: the entry value

  The function will perform the update and return the updated todo-list.

  ## Examples

      iex> (
      ...>  TodoList.new
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
      ...>  |> TodoList.update_entry(%{id: 3, date: ~D[2018-12-20], title: "movie"})
      ...>  |> TodoList.entries(~D[2018-12-20])
      ...> )
      [
        %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
        %{date: ~D[2018-12-20], id: 3, title: "movie"}
      ]
  """
  def update_entry(%TodoList{} = todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  @doc """
  Modify a single entry in the to-do list.

  - `entry_id`:  ID value of the entry to be updated.
  - `update_fn`: The update function to be performed on the entry.

  The function will perform the update and return the updated todo-list.

  No error is raised if the ID value doesn't exist. Instead the unchanged todo-list is returned.

  ## Examples

      iex> (
      ...>  TodoList.new
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
      ...>  |> TodoList.update_entry(3, &Map.put(&1, :date, ~D[2018-12-20]))
      ...>  |> TodoList.entries(~D[2018-12-20])
      ...> )
      [
        %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
        %{date: ~D[2018-12-20], id: 3, title: "Movies"}
      ]
  """
  def update_entry(%TodoList{} = todo_list, entry_id, update_fn)
      when is_number(entry_id) and is_function(update_fn, 1) do
    case Map.fetch(todo_list.entries, entry_id) do
      # no entry - return the unchanged list
      :error ->
        todo_list

      # @see discussion: /docs/immutable-hierarchical-updates-*.png

      # entry exists - update and return new list
      {:ok, old_entry} ->
        # new entry must be a Map with the ID unchanged
        old_entry_id = old_entry.id
        new_entry = %{id: ^old_entry_id} = update_fn.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %TodoList{todo_list | entries: new_entries}
    end
  end

  @doc """
  Delete a single entry from the to-do list.

  - `entry_id`:  ID value of the entry to be deleted.

  The function will perform the delete and return the updated todo-list.

  No error is raised if the ID value doesn't exist. Instead the unchanged todo-list is returned.

  ## Examples

      iex> (
      ...>  TodoList.new
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Shopping"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
      ...>  |> TodoList.delete_entry(2)
      ...>  |> TodoList.entries(~D[2018-12-19])
      ...> )
      [
        %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
        %{date: ~D[2018-12-19], id: 3, title: "Movies"}
      ]
  """
  def delete_entry(%TodoList{} = todo_list, entry_id) when is_number(entry_id) do
    %TodoList{todo_list | entries: Map.delete(todo_list.entries, entry_id)}
  end

  @doc """
  Return all entries from the to-do list for the given `date`, or an empty list if no entries exist for that `date`.

  ## Examples

      iex> (
      ...>  TodoList.new
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      ...>  |> TodoList.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
      ...>  |> TodoList.entries(~D[2018-12-19])
      ...> )
      [
        %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
        %{date: ~D[2018-12-19], id: 3, title: "Movies"}
      ]
  """
  # entry example: %{1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"}}
  def entries(%TodoList{} = todo_list, date) do
    # 1st transformation is done using the Stream module, whereas
    # the 2nd transformation uses the Enum module.
    # This allows both transformations to happen in a single pass through the todo_list
    todo_list.entries

    # take only those entries that fall on the given date.
    |> Stream.filter(fn {_id, entry} -> entry.date == date end)

    # extract only the entry element
    |> Enum.map(fn {_id, entry} -> entry end)
  end
end
