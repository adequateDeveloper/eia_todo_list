defmodule DbServer do
  @moduledoc """
  An exercise in Separation of Concerns between client and server process visibility.

  Modeled on ideas presented by Dave Thomas in his course [Elixir for Programmers](https://pragdave.me/).

  Note that all module functions run in the client process.

  ## Examples

      $ iex -S mix
      iex> server_pid = DbServer.start()
      iex> DbServer.run_async(server_pid, "query 1")
      iex > DbServer.get_result()
      "Connection 753: query 1 result"
      iex> DbServer.run_async(server_pid, "query 2")
      iex> DbServer.get_result()
      "Connection 753: query 2 result"
  """

  alias DbServer.Impl

  @doc """
  Start a server process.
  """
  @spec start() :: pid()
  defdelegate start, to: Impl

  @doc """
  Request an asyncronous query operation.
  """
  @spec run_async(pid(), any()) :: any()
  defdelegate run_async(server_pid, query_def), to: Impl

  @doc """
  Retrieve server response.
  """
  @spec get_result() :: any()
  defdelegate get_result, to: Impl
end
