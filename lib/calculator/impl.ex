defmodule Calculator.Impl do
  @moduledoc """
  See DbServer.Impl moduledoc
  """

  @compile if Mix.env() == :test, do: :export_all

  ## delegated client calls

  @doc """
  Start a server process with initial state and
  responds to calculator messages.
  """
  @spec start() :: pid()
  def start do
    spawn(fn -> loop(0) end)
  end

  @doc """
  Request the current value from the server process.
  A synchronous call.
  """
  def value(server_pid) do
    send(server_pid, {:value, self()})

    receive do
      {:response, value} -> value
    end
  end

  @doc """
  Arithmetic operation.
  A asynchronous call.
  """
  def add(server_pid, value) when is_integer(value), do: send(server_pid, {:add, value})

  @doc """
  Arithmetic operation.
  A asynchronous call.
  """
  def sub(server_pid, value) when is_integer(value), do: send(server_pid, {:sub, value})

  @doc """
  Arithmetic operation.
  A asynchronous call.
  """
  def mul(server_pid, value) when is_integer(value), do: send(server_pid, {:mul, value})

  @doc """
  Arithmetic operation.
  A asynchronous call.
  """
  def div(server_pid, value) when is_integer(value), do: send(server_pid, {:div, value})

  ## server implementation

  defp loop(current_value) do
    new_value =
      receive do
        message ->
          process_message(current_value, message)
      end

    loop(new_value)
  end

  defp process_message(current_value, {:value, caller}) do
    send(caller, {:response, current_value})
    current_value
  end

  defp process_message(current_value, {:add, value}), do: current_value + value
  defp process_message(current_value, {:sub, value}), do: current_value - value
  defp process_message(current_value, {:mul, value}), do: current_value * value
  defp process_message(current_value, {:div, value}), do: current_value / value

  defp process_message(current_value, invalid_message) do
    IO.puts("invalid request #{inspect(invalid_message)}")
    current_value
  end
end
